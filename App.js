import React, { useEffect } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useColorScheme, View, Button } from 'react-native';
import { Colors, Header} from 'react-native/Libraries/NewAppScreen';

import MyCustomModule from './CustomModule';

const App = () => {

  MyCustomModule.showToast();

  const renderPakacageScreen = () => {
    MyCustomModule.callCustomLibraryScreen();
  }

  return (
    <SafeAreaView style={{backgroundColor: Colors.light}}>
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={{backgroundColor: Colors.light}}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={{backgroundColor: Colors.light}}>
        <Header />
        <View
          style={{
            backgroundColor:  Colors.white,
          }}>        
          <Section title=""
              style={{marginTop: 10, }}          
          >
            <Button
              onPress={renderPakacageScreen}
              title="Render Native Package Screen"
            />
          </Section>       

        </View>
      </ScrollView>
    </SafeAreaView>
  );

 
};

/* $FlowFixMe[missing-local-annot] The type annotation(s) required by Flow's
 * LTI update could not be added via codemod */
const Section = ({children, title}) => {
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: Colors.white,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: Colors.light,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
