# RNAndroid

This is a React Native project, which uses a react native bridge to communicate between android native and javascript.

This application use Android Archive(AAR) library in it's custom module inside android folder.

We use MyLibrary package(.aar) as a dependency in /android/app/build.gradle file

## MyLibrary

This is android native package which is used to create Android Archive(AAR) file

[MyLibrary Repository](https://gitlab.com/manishd-coditation/mylibrary)

## Demo

Working Demo

![Demo 2](app-demo.gif)
