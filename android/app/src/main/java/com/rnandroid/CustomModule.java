package com.rnandroid;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.mylibrary.ScreenOneActivity;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class CustomModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;
    Context context;

    CustomModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
        this.context = context.getApplicationContext(); // This is where you get the context
    }

    @NonNull
    @Override
    public String getName() {
        return "MyCustomModule";
    }

    @ReactMethod
    public void showToast() {
        Toast.makeText(reactContext, "Hi from Android!!!", Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void callCustomLibraryScreen() {
        getCurrentActivity().runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        Intent lIntent = new Intent(context, ScreenOneActivity.class);
                        lIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(lIntent);
                    }
                });
    }

}